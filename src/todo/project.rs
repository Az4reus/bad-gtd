use todo::task::Task;

pub struct Project {
    pub title: String,
    pub project_id: i32,
    pub tasks: Vec<Task>
}

impl Project {
    pub fn new(title: &str, project_id: i32) -> Project {
        Project {
            title:      title.to_string(),
            project_id: project_id,
            tasks:      Vec::<Task>::new()
        }
    }

    /// Adds a new Task to a project with just title and description,
    /// just a nice little composition function.
    pub fn add_task(&self, title: &str) -> Project {
        let index = self.get_next_pid();
        let task = Task::new(title, index);
        self.add_raw_task(task)
    }

    fn add_raw_task(&self, task: Task) -> Project {
        let mut new_tasks = self.tasks.clone();
        new_tasks.push(task);

        Project {
            title:       self.title.clone(),
            project_id:  self.project_id.clone(),
            tasks:       new_tasks,
        }
    }

    fn get_next_pid(&self) -> i32 {
        (self.tasks.len() + 1) as i32
    }

    /// Takes a PID of a task contained within a project, marks that task as done,
    /// then returns a new Project with the task marked as done. Performs check
    /// that only one Task in the project has the selected PID.
    pub fn do_task(&self, pid: i32) -> Project {
        let mut tasks = self.tasks.clone();

        // We only need the (ideally sole) task with the matching PID.
        tasks.retain(|t| t.pid == pid);

        // Check if there's only one task with that that number,
        // else, we're fucked.
        assert!(tasks.len() == 1);

        // consume the Iterator and grab the only element.
        let actual_task: Option<&Task> = tasks.first();

        // No need for pattern matching due to previously checked
        // pid and length.
        let done_task = actual_task.unwrap().do_task();

        // Grab the original set of tasks.
        let mut original_tasks = self.tasks.clone();

        // Find the position of the task in the original set
        let index = original_tasks
            .iter()
            .position(|t| t.pid == pid)
            .unwrap();

        // Actually remove the superfluous element.
        original_tasks.remove(index);

        // Add the Task at the spot it was removed from.
        original_tasks.insert(index, done_task);

        // Then, return the new Project.
        Project {
            title:      self.title.clone(),
            project_id: self.project_id,
            tasks:           original_tasks,
        }
    }

    pub fn print(&self) -> () {
        use std::iter;

        let header = format!("{project_id:03} | {project_title:<72}\n",
            project_id = self.project_id,
            project_title = self.title);

        let separator: String = iter::repeat("=").take(80).collect::<String>();

        println!("{}{}", header, separator);

        // Aaaand printing all the tasks, or rather letting them handle that.
        for task in self.tasks.clone() {
            task.print_oneline()
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use todo::task::Task;

    #[test]
    fn test_creation() {
        let new_project = Project::new("test project", 1);
        assert!(new_project.title == "test project");
        assert!(new_project.project_id == 1);
        assert!(new_project.tasks == Vec::<Task>::new());
    }

    #[test]
    fn test_adding_tasks() {
        let project = Project::new("another test project", 1);
        let project_with_task = project.add_task("test task");
        let task = project_with_task.tasks.first().unwrap();
        assert!(task.title == "test task");
        assert!(task.pid == 1);
    }

    #[test]
    fn test_doing_tasks() {
        let project = Project::new("yet another test project", 1);
        let project_with_tasks = project.add_task("test task, the second.");
        let project_done_task = project_with_tasks.do_task(1);
        let task = project_done_task.tasks.first().unwrap();

        assert!(task.is_done == true);
        assert!(task.title == "test task, the second.");
    }

    #[test]
    fn test_printing() {
        let project = Project::new("Test project", 1);
        let project_with_tasks = project
            .add_task("test 1")
            .add_task("test 2")
            .add_task("test 3");

        println!("Does this look okay to you?");
        project_with_tasks.print();
    }
}
