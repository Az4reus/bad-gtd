use chrono::*;

#[derive(Clone)]
pub struct Task {
    pub title: String,
    pub is_done: bool,
    pub created: DateTime<UTC>,
    pub due: Option<DateTime<UTC>>,
    pub pid: i32
}

impl Task {
    pub fn do_task(&self) -> Task {
        Task {
            title:       self.title.clone(),
            is_done:     true,
            created:     self.created.clone(),
            due:         self.due.clone(),
            pid:         self.pid,
        }
    }

    pub fn new(title: &str, npid: i32) -> Task {
        Task {
            title:       title.to_string(),
            is_done:     false,
            created:     UTC::now(),
            due:         None,
            pid:         npid,
        }
    }

    pub fn set_due(&self, due_time: DateTime<UTC>) -> Task {
        Task {
            title:       self.title.clone(),
            is_done:     self.is_done,
            created:     self.created.clone(),
            due:         Some(due_time),
            pid:         self.pid,
        }
    }

    pub fn print_oneline(&self) -> () {
        // TODO implement `due` here.
        println!("{task_id:03} | {task_title:<70} |{done}|",
            task_id = self.pid,
            task_title = self.title,
            done = if self.is_done {"#"} else {" "});
    }
}

impl PartialEq for Task {
    fn eq(&self, other: &Task) -> bool {
        if self.title   == other.title
        && self.is_done == other.is_done
        && self.pid     == other.pid {
            true
        } else { false }
    }
}
