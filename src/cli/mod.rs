use database;
use todo::project::Project;

pub fn parse_args(args: Vec<String>) {
    let first_arg = &args[1];
    match &first_arg[..] {
        "add"    => add_handler(&args),
        "do"     => do_handler(&args),
        "select" => select_handler(&args),
        _ => println!("Invalid Command, fix it, dummy.")
    };
}

fn add_handler(args: &Vec<String>) {
    let db_conn = database::init_db();
    let size = args.len();
    let title = args[2..size].join(" ");
    let current_project: Project = database::get_current_project(&db_conn);

    let new_project = current_project.add_task(&title);
    match database::replace_project(&db_conn, &new_project){
        Ok(_)  => {},
        Err(x) => panic!("Shit died when replacing a project, Error: {}", x)
    };

    new_project.print();
    println!("\nAdded task.");
}

fn do_handler(args: &Vec<String>) {

}

fn select_handler(args: &Vec<String>) {

}