extern crate rusqlite;
extern crate chrono;

pub mod deserialiser;

use todo::project::Project;
use todo::task::Task;
use self::rusqlite::{Connection, Error};
use self::chrono::{DateTime, UTC};

pub fn init_db() -> Connection {
    let conn = Connection::open(::DB_PATH).unwrap();
    let table_check = check_tables(&conn);

    // If all the tables we want are present, return the connection,
    // else re-initiate the missing tables and then return the connection.
    // if that doesn't work, welp, good luck!
    match table_check {
        Ok(())    => conn,
        Err(_)    => match init_tables(&conn){
            Ok(_)  => conn,
            Err(_) => panic!("Shit's fucked up, can't create tables.")
        }
    }
}

fn init_tables(conn: &Connection) -> Result<i32, Error> {
    conn.execute("
        CREATE TABLE IF NOT EXISTS projects (
            title      TEXT,
            project_id INT PRIMARY KEY ASC);

        INSERT INTO projects VALUES ('inbox', 0);

        CREATE TABLE IF NOT EXISTS tasks (
            title   TEXT,
            status  INT,
            created TEXT,
            due     TEXT,
            pid     INT
            project INT);
        ", &[])
}

fn check_tables(conn: &Connection) -> Result<(), Error> {
    let query = format!("SELECT name FROM {}.sqlite_master WHERE type='table';",
        ::DB_PATH);
    let tables = conn.execute(&query, &[]);

    Ok(())
}

pub fn get_current_project(conn: &Connection) -> Project {

    // Grab the data from the current_project table, which only has one row.
    let mut statement = conn.prepare("select * from current_project").unwrap();
    let mut rows = statement.query(&[]).unwrap();
    let raw_data = rows.next().unwrap().unwrap();

    // Try and reconstruct a project from that data, else grab the inbox.
    let ret = deserialiser::reconstruct_project(raw_data);
    match ret {
        Ok(x) => x,

        // If this doesn't succeed, we're fucked anyways,
        // as it should be handled by the db function.
        Err(_) => get_project(conn, 0).unwrap()
    }
}

pub fn replace_project(conn: &Connection, new_project: &Project)
-> Result<i32, Error> {
    let update_result = update_tasks(conn, &new_project.tasks, new_project.project_id);
    match update_result {
        Ok(()) => {},
        Err(x) => panic!("Things died while replacing a project,
            could not replace tasks, Error: {}", x)
    };

    conn.execute(
        "UPDATE projects
         SET title=$2
         WHERE project_id=$1;",
        &[&new_project.project_id,
          &new_project.title])
}

fn update_tasks(conn: &Connection, tasks: &Vec<Task>, parent_project_id: i32)
-> Result<(), Error> {

    // This feels pretty wonky to write -- if something goes wrong,
    // it's probably here, with things not getting updated properly.
    let mut statement = conn.prepare(
        "UPDATE tasks
        SET (title = $1,
             status = $2,
             due = $3,
             pid = $4, )
        WHERE project=$5 AND pid=$4").unwrap();

    for t in tasks {
        match statement.query(&[
            &t.title,
            &t.is_done,
            &timestamp(t.due.clone()),
            &t.pid,
            &parent_project_id]) {
                Ok(_) => {},
                Err(x) => return Err(x)
        };
    }

    Ok(())
}

fn timestamp(time: Option<DateTime<UTC>>) -> String {
    match time {
        Some(x) => x.format("%Y-%m-%d %H:%M:%S").to_string(),
        None    => String::new()
    }
}

pub fn set_current_project(conn: &Connection, project_id: i32)
-> Result<i32, Error> {
    let project = get_project(conn, project_id).unwrap();
    let update_result = update_tasks(conn, &project.tasks, project.project_id);

    match update_result {
        Ok(_)  => {},
        Err(x) => panic!("Could not update tasks, Error: {}", x)
    }

    conn.execute(
        "UPDATE current_project
        SET project_id=$1, title=$2
        WHERE id=1",
        &[&project.project_id,
          &project.title])
}

pub fn get_project(conn: &Connection, project_id: i32) -> Result<Project, Error> {
    Ok(Project::new("Placeholder project", 0))
}