extern crate rusqlite;

use todo::project::Project;
use self::rusqlite::Error;
use self::rusqlite::Row;


pub fn reconstruct_project(datavector: Row) -> Result<Project, Error> {
    Ok(Project::new("placeholder project", 0))
}