extern crate chrono;

pub mod todo;
pub mod cli;
pub mod database;

const DB_PATH: &'static str = "bad-gtd.db";


fn main() {
    let args: Vec<String> = std::env::args().collect();
    cli::parse_args(args);
}

#[cfg(test)]
mod test{

    #[test]
    fn sanity_check() {
        assert!(1+1 == 2);
    }
}